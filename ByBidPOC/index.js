/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */
import React from "react"; //necesario para Redux
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

import { Provider } from "react-redux"; 
import configureStore from "./src/store/configureStore"; //necesario para Redux

const store = configureStore();

const RNRedux = () => (
  <Provider store={store}>
      <App />
  </Provider>
);

AppRegistry.registerComponent(appName, () => RNRedux);
