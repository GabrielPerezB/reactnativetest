import React from "react";
import {
  TouchableOpacity,
  TouchableNativeFeedback,
  Text,
  View,
  StyleSheet,
  Platform
} from "react-native";

const buttonWithBackground = props => {
  const content = (
    <View style={styles.button}>
      <Text style={styles.text}>{props.children}</Text>
    </View>
  );

  if (Platform.OS === "android") {
    return (
      <TouchableNativeFeedback onPress={props.onPress}>
        {content}
      </TouchableNativeFeedback>
    );
  }
  return <TouchableOpacity onPress={props.onPress}>{content}</TouchableOpacity>;
};

const styles = StyleSheet.create({
  button: {
    padding: 8,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: "#357ebd",
    width: "100%",
    height: 36,
    backgroundColor: "#428bca"
  },
  text: {
    color: "#fff",
    textAlign: "center",
    fontFamily: "inherit",
    fontWeight: "400",
  }
});

export default buttonWithBackground;
