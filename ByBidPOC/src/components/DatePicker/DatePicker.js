import React, { Component } from "react";
import DatePicker from "react-native-datepicker";

export default class MyDatePicker extends Component {
  constructor(props) {
    super(props);
    this.state = { selectedStartDate: null };
  }

  render() {
    const { selectedStartDate } = this.state;
    const startDate = selectedStartDate ? selectedStartDate.toString() : "";
    const minDate = new Date(); // Today
    return (
      <DatePicker
        style={{ width: 200 }}
        date={this.state.selectedStartDate}
        mode="date"
        placeholder="select date"
        format="MM/DD/YYYY"
        minDate={minDate}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: "absolute",
            left: 0,
            top: 4,
            marginLeft: 0
          },
          dateInput: {
            marginLeft: 36
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={date => {
          this.setState({ selectedStartDate: date });
        }}
      />
    );
  }
}
