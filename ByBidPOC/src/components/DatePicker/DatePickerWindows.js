import React, { Component } from "react";
import { Text, View, Modal, StyleSheet, ToastAndroid } from "react-native";
import { Calendar } from "react-native-calendars";

import CalendarPicker from "react-native-calendar-picker";
import moment from "moment";

class DatePicker extends Component {
  /*constructor(props) {
    super(props);
    this.state = {
      selectedStartDate: null
    };

    this.onDateChange = this.onDateChange.bind(this);
  }
/*
  onDateChange(date) {
    const dateParsed = moment(date).format("L");
    alert(dateParsed);
    this.setState({
      selectedStartDate: date
    });
  }

  render() {
    const { selectedStartDate } = this.state;
    const startDate = selectedStartDate ? selectedStartDate.toString() : "";
    const minDate = new Date(); // Today
    return (
      <Modal
        animationType="slide"
        transparent={true}
        {...this.props}
        onRequestClose={this.props.setModalVisible}
      >
        <View style={{ width: 300, height:300 }}>
          <View style={{ flex: 1, backgroundColor: "#FFFFFF", margin: 20}}>
            <CalendarPicker
              minDate={minDate}
              selectedDayColor="#428bca"
              onDateChange={this.onDateChange}
            />

            <View>
              <Text>SELECTED DATE:{startDate}</Text>
              
            </View>
          </View>
        </View>
      </Modal>
    );
  }*/

  constructor(props) {
    super(props);
    this.state = {
      selectedStartDate: null
    };
  }

  render() {
    const { selectedStartDate } = this.state;
    const startDate = selectedStartDate ? selectedStartDate.toString() : "";
    const minDate = new Date(); // Today
    return (
      <View style={{ width: 300, height: 300 }}>
        <Calendar
          // Initially visible month. Default = Date()
          current={this.state.selectedStartDate}
          // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
          minDate={minDate}
          // Handler which gets executed on day press. Default = undefined
          onDayPress={day => {
            console.log("selected day", day);
          }}
          monthFormat={"MM/dd/yyyy"}
          hideExtraDays={true}
          // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
          firstDay={0}
          onPressArrowLeft={substractMonth => substractMonth()}
          onPressArrowRight={addMonth => addMonth()}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  button: {
    marginTop: 30
  },
  form: {
    padding: 20
  },
  content: {
    flex: 1,
    flexDirection: "row"
  }
});

export default DatePicker;
