import { SET_CARS } from "../actions/actionTypes";

const initialState = {
  cars: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CARS:
      alert(JSON.stringify(action.cars));
      return {
        ...state,
        cars: action.cars
      };
    default:
      return state;
  }
};

export default reducer;
