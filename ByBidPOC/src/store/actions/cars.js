import {  SET_CARS } from "./actionTypes";
import XMLParser from "react-xml-parser";
import parseString from "react-native-xml2js";
//const parseString = require('react-native-xml2js').parseString;

export const getCars = () => {
      return dispatch => {
      fetch("https://7k7xve6dec.execute-api.us-east-2.amazonaws.com/default/python-lambda")
        .catch(err => {
          alert("Can't load Cars");
        })
        .then(res => res.json())
        .then(parsedRes => {
          const cars = [];
          for (let car in parsedRes) {
            cars.push({
              ...parsedRes[car],
              key: car.model
            });
          }
          dispatch(setCars(cars));
        });
    };

    //XML
    /*return dispatch => {
      fetch("https://7k7xve6dec.execute-api.us-east-2.amazonaws.com/default/python-lambda")
        .catch(err => {
          alert("Can't load Cars");
        })
        .then(response => response.text())
        .then(response => {
          let xml = new XMLParser().parseFromString(response);
          dispatch(setCars(xml));
        });
    };*/
  };
  
  export const setCars = cars => {
    return {
      type: SET_CARS,
      cars: cars
    };
  };
  