import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView
} from "react-native";
import { getCars } from "../../store/actions/index";
import { connect } from "react-redux";
import CheckBox from "react-native-check-box";
import { NativeRouter, Route, Link, Redirect } from "react-router-native";
import DefaultInput from "../../components/UI/DefaultInput/DefaultInput";
import ButtonWithBackground from "../../components/UI/ButtonWithBackground/ButtonWithBackground";
import ConsignUnits from "../ConsignUnits/ConsignUnits";

class Login extends Component {
  state = {
    checked: false,
    loggedIn: false
  };
  

  onLoginClick = () => {
    //this.props.onGetCars();
    this.setState({
      loggedIn: !this.state.loggedIn
    });
  };

  LoginView = () => {
    return (
      <View style={styles.container}>
        <Image
          source={{
            uri:
              "https://s3.us-east-2.amazonaws.com/buybidstatics/img/logobig.png"
          }}
          style={styles.image}
        />
        <View style={styles.form}>
          <Text style={styles.textLogin}>Login</Text>
          <DefaultInput
            placeholder="Username"
            style={[
              styles.input,
              {
                borderBottomRightRadius: 0,
                borderBottomLeftRadius: 0
              }
            ]}
            //value={"email"}
            // onChangeText={val => this.updateInputState("email", val)}
            //valid={this.state.controls.email.valid}
            //touched={this.state.controls.email.touched}
            autoCapitalize="none"
            autoCorrect={false}
          />
          <DefaultInput
            placeholder="Password"
            style={[
              styles.input,
              {
                borderTopRightRadius: 0,
                borderTopLeftRadius: 0
              }
            ]}
            //value={"email"}
            // onChangeText={val => this.updateInputState("email", val)}
            //valid={this.state.controls.email.valid}
            //touched={this.state.controls.email.touched}
            autoCorrect={false}
            secureTextEntry
          />
        </View>

        <View style={styles.checkBox}>
          <CheckBox
            onClick={() => {
              this.setState({
                isChecked: !this.state.isChecked
              });
            }}
            isChecked={this.state.isChecked}
          />
          <Text style={styles.rememberMe}>Remember me</Text>
        </View>

        <View style={{ width: "80%" }}>
          <ButtonWithBackground onPress={this.onLoginClick}>
            Login
          </ButtonWithBackground>
        </View>
      </View>
    );
  };

  ConsignUnitsView = () => {
    return <ConsignUnits redirect={this.onLoginClick}/>;
  };

  render() {
    return (
      <NativeRouter>
        <ScrollView>
          <Route
            exact
            path="/"
            render={() =>
              this.state.loggedIn ? <Redirect to="/consignUnits" /> : null
            }
          />
          <Route exact path="/" component={this.LoginView} />
          <Route path="/consignUnits" component={this.ConsignUnitsView} />
        </ScrollView>
      </NativeRouter>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    justifyContent: "flex-start",
    alignItems: "center"
  },
  image: { width: 150, height: 200 },
  input: {
    backgroundColor: "white",
    fontFamily: "inherit",
    borderRadius: 3,
    height: 36
  },
  form: {
    width: "80%"
  },
  textLogin: {
    fontFamily: "inherit",
    fontWeight: "400",
    fontSize: 18,
    marginTop: 5,
    marginBottom: 8
  },
  rememberMe: {
    marginTop: 2,
    fontFamily: "inherit",
    fontSize: 16
  },
  checkBox: {
    flexDirection: "row",
    justifyContent: "flex-start",
    width: "80%",
    marginTop: 10
  }
});

const mapStateToProps = state => {
  return {
    cars: state.cars.cars
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetCars: () => dispatch(getCars())
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
