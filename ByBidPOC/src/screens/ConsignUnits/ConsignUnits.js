import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Button
} from "react-native";
import { getCars } from "../../store/actions/index";
import { connect } from "react-redux";
import ImagePickerWindows from "../../components/ImagePicker/imagePickerWindows";
import ImagePicker from "../../components/ImagePicker/imagePicker";
import DatePickerWindows from "../../components/DatePicker/DatePickerWindows";
import ButtonWithBackground from "../../components/UI/ButtonWithBackground/ButtonWithBackground";

import DatePicker from "../../components/DatePicker/DatePicker";
import RadioForm from "react-native-simple-radio-button";

import DefaultInput from "../../components/UI/DefaultInput/DefaultInput";

import { Route, Redirect } from "react-router-native";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vehicleClassifications: [
        { label: "Sealed Bid/Auction", value: 0 },
        { label: "Sealed Bid Only", value: 1 }
      ],
      inspectionTeam: "",
      startDate: "",
      endDate: "",
      OdometerReading:"",
      vinNumber:"",
      pictures=[],
      loggedIn: true,
      value: 0,
      modalStartDataVisible: true
    };
  }

  onLogOut = () => (
    this.setState({ loggedIn: false }),
    this.props.redirect,
    <Redirect push to="/" />
  );

  modalStartDateHandler = () => {
    this.setState({
      modalStartDataVisible: !this.state.modalStartDataVisible
    });
  };

  render() {
    let startDatePicker = null;
    let endDatePicker = null;
    let imagePicker = null;
    Platform.OS === "android"
      ? ((imagePicker = <ImagePicker />),
        (startDatePicker = <DatePicker />),
        (endDatePicker = <DatePicker />))
      : ((imagePicker = <ImagePickerWindows />),
        (startDatePicker = (
          <View>
            <DefaultInput
              underlineColorAndroid="#000000"
              //value={"email"}
              // onChangeText={val => this.updateInputState("email", val)}
              //valid={this.state.controls.email.valid}
              //touched={this.state.controls.email.touched}
              autoCapitalize="none"
              autoCorrect={true}
              style={{ width: 300 }}
            />
            <DatePickerWindows
              visible={this.state.modalStartDataVisible}
              setModalVisible={this.modalStartDateHandler}
              onDatePicker={this.onDatePicker}
            />
          </View>
        )),
        (endDatePicker = (
          <View>
            <DefaultInput
              underlineColorAndroid="#000000"
              //value={"email"}
              // onChangeText={val => this.updateInputState("email", val)}
              //valid={this.state.controls.email.valid}
              //touched={this.state.controls.email.touched}
              autoCapitalize="none"
              autoCorrect={true}
              style={{ width: 300 }}
            />
            <DatePickerWindows
              visible={this.state.modalStartDataVisible}
              setModalVisible={this.modalStartDateHandler}
              onDatePicker={this.onDatePicker}
            />
          </View>
        )));

    return (
      <ScrollView>
        <Route
          exact
          path="/consignUnits"
          render={() => (!this.state.loggedIn ? <Redirect to="/" /> : null)}
        />
        <View style={styles.container}>
          <View style={styles.header}>
            <Image
              source={{
                uri:
                  "https://s3.us-east-2.amazonaws.com/buybidstatics/img/logocarFL.png"
              }}
              style={styles.image}
            />
            <View style={styles.logOut}>
              <Text>Log Out</Text>
            </View>
          </View>
          <Text style={{ alignSelf: "center", fontSize: 24 }}>
            Consign Units
          </Text>
          <View style={styles.form}>
            <View style={styles.formComponent}>
              <Text>Vehicle Classification*:</Text>
              <RadioForm
                style={{ marginRight: 50 }}
                radio_props={this.state.vehicleClassifications}
                initial={0}
                formHorizontal={true}
                labelHorizontal={true}
                borderWidth={1}
                buttonSize={7}
                buttonColor={"#bbb"}
                selectedButtonColor={"#428bca"}
                buttonOuterSize={20}
                animation={true}
                onPress={value => {
                  this.setState({ ...this.state, value: value });
                }}
              />
            </View>

            <View style={styles.formComponent}>
              <Text>Inspection Team*:</Text>
              <DefaultInput
                underlineColorAndroid="#000000"
                //value={"email"}
                // onChangeText={val => this.updateInputState("email", val)}
                //valid={this.state.controls.email.valid}
                //touched={this.state.controls.email.touched}
                autoCapitalize="none"
                autoCorrect={true}
              />
            </View>

            <View style={[styles.formComponent, { marginBottom: 50 }]}>
              <Text>Auction Start Date*:</Text>
              {startDatePicker}
            </View>

            <View style={[styles.formComponent, { marginBottom: 50 }]}>
              <Text>Auction End Date*:</Text>
              {endDatePicker}
            </View>

            <View style={styles.formComponent}>
              <Text>Odometer Reading*:</Text>
              <DefaultInput
                underlineColorAndroid="#000000"
                //value={"email"}
                // onChangeText={val => this.updateInputState("email", val)}
                //valid={this.state.controls.email.valid}
                //touched={this.state.controls.email.touched}
                autoCapitalize="none"
                autoCorrect={true}
              />
            </View>

            <View style={styles.formComponent}>
              <Text>Vin Number*:</Text>
              <DefaultInput
                underlineColorAndroid="#000000"
                //value={"email"}
                // onChangeText={val => this.updateInputState("email", val)}
                //valid={this.state.controls.email.valid}
                //touched={this.state.controls.email.touched}
                autoCapitalize="none"
                autoCorrect={true}
              />
            </View>

            <Text style={{ alignSelf: "center", fontSize: 24 }}>
              Attach Pictures
            </Text>
            <View style={styles.formComponent}>
              <Text style={{alignSelf:"center"}}>Pictures 20 Max.</Text>
              {imagePicker}
            </View>
          </View>

          <View style={styles.formComponent}>
          <ButtonWithBackground onPress={this.onSubmit}>
            Click here to send units to auction
          </ButtonWithBackground>
            </View>
          
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    justifyContent: "flex-start",
    alignItems: "center"
  },
  image: {
    width: 110,
    height: "90%",
    marginBottom: 10
  },
  input: {
    backgroundColor: "white",
    fontFamily: "inherit",
    borderRadius: 3,
    height: 36
  },
  form: {
    width: "80%"
  },
  textLogin: {
    fontFamily: "inherit",
    fontWeight: "400",
    fontSize: 18,
    marginTop: 5,
    marginBottom: 8
  },
  header: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-end",
    width: "100%",
    height: 80
  },
  logOut: {
    alignSelf: "flex-end",
    marginBottom: 10
  },
  formComponent: {
    marginBottom: 10
  }
});

const mapStateToProps = state => {
  return {
    cars: state.cars.cars
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetCars: () => dispatch(getCars())
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
